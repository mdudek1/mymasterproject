package com.example.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.user.User;
import java.util.List;

public class UsersAdapter extends ArrayAdapter<User> {

    private List<User> users;
    private Context context;

    public UsersAdapter(@NonNull List<User> users, Context context){
        super(context, R.layout.user_layout, users);
        this.users = users;
        this.context = context;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.user_layout, parent, false );
        User user = this.users.get(position);
        TextView textViewName = (TextView) view.findViewById(R.id.textViewName);
        textViewName.setText("Name: " + user.getName());
        TextView textViewSetHour = (TextView) view.findViewById(R.id.textViewSetHour);
        textViewSetHour.setText("Set hour: " + String.valueOf(user.getSetHour()));
        TextView textViewSetTemperature = (TextView) view.findViewById(R.id.textViewSetTemperature);
        textViewSetTemperature.setText("Set temperature: " + String.valueOf(user.getSetTemperature()));
        return view;
    }
}
