package com.example.user;

import java.io.Serializable;

public class User implements Serializable {

    private int ID;
    private String name;
    private double setHour;
    private int setTemperature;


    public User() {
    }

    public User(int ID, String name, double setHour, int setTemperature) {
        this.ID = ID;
        this.name = name;
        this.setHour = setHour;
        this.setTemperature = setTemperature;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSetHour() {
        return setHour;
    }

    public void setSetHour(double setHour) {
        this.setHour = setHour;
    }

    public int getSetTemperature() {
        return setTemperature;
    }

    public void setSetTemperature(int setTemperature) {
        this.setTemperature = setTemperature;
    }


}
