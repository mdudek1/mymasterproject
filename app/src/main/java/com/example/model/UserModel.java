package com.example.model;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;

import com.example.config.ConfigReader;
import com.example.user.User;

import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import java.util.*;

public class UserModel {

    //HashMap<String, String> map = new HashMap<>();
    //private Context context;
    //private ConfigReader reader = new ConfigReader(map, this);

    private String BASE_URL = /*reader.getValue("BASE_URL");*/ "http://172.22.1.79";
    private RestTemplate restTemplate = new RestTemplate();

    public List<User> read(){
        try {
            return restTemplate.exchange(
                    BASE_URL+":8085/users",
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<User>>() {
                    }
            ).getBody();
        }catch(Exception e){
            return null;
        }
    }

    public boolean create(User user){
        try {
            Map<String, String> values = new HashMap<>();
            values.put("name", user.getName());
            values.put("setHour", String.valueOf(user.getSetHour()));
            values.put("setTemperature", String.valueOf(user.getSetTemperature()));
            JSONObject jsonObject = new JSONObject(values);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
            restTemplate.postForEntity(BASE_URL + "/create", entity, null);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public boolean delete (int id){
        try{
            restTemplate.delete(BASE_URL + "/delete/" + "{" + id + "}" );
            return true;
        }catch (Exception e){
            return false;
        }
    }
    public boolean update (User user) {
        try {
            Map<String, String> values = new HashMap<>();
            values.put("id",String.valueOf(user.getID()));
            values.put("name", user.getName());
            values.put("setHour", String.valueOf(user.getSetHour()));
            values.put("setTemperature", String.valueOf(user.getSetTemperature()));
            JSONObject jsonObject = new JSONObject(values);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
            restTemplate.put(BASE_URL + "/update", entity);
            return true;
        }catch(Exception e){
            return false;
        }
    }

}
