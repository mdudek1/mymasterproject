package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.config.ConfigReader;
import com.jiongbull.jlog.*;
import com.jiongbull.jlog.constant.LogLevel;
import com.jiongbull.jlog.constant.LogSegment;
import com.jiongbull.jlog.util.TimeUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends Activity {

    private Logger sLogger;
    String name;
    EditText editText;
    Button button;git a
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            Class cls = Class.forName("android.sec.clipboard.ClipboardUIManager");
            Method m = cls.getDeclaredMethod("getInstance", Context.class);
            m.setAccessible(true);
            m.invoke(null, this);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        List<String> logLevels = new ArrayList<>();
        logLevels.add(LogLevel.DEBUG);
        logLevels.add(LogLevel.INFO);
        logLevels.add(LogLevel.WARN);
        logLevels.add(LogLevel.ERROR);
        logLevels.add(LogLevel.WTF);

        HashMap<String, String> map = new HashMap<>();

        ConfigReader test = new ConfigReader(map, getApplicationContext());
        Log.d("Parameters", test.toString() );

        boolean debugToConsoleValue = Boolean.parseBoolean(test.getValue("DebugToConsole"));
        boolean debugToFileValue = Boolean.parseBoolean((test.getValue("DebugToFile")));

        sLogger = Logger.Builder.newBuilder(getApplicationContext(), "jlog")

                .setDebug(debugToConsoleValue)
                .setWriteToFile(debugToFileValue)
                // properties below are of default values
                .setLogDir("jlog")
                .setLogPrefix("")
                .setLogSegment(LogSegment.TWELVE_HOURS)
                .setLogLevelsForFile(logLevels)
                .setZoneOffset(TimeUtils.ZoneOffset.P0100)
                .setTimeFormat("yyyy-MM-dd HH:mm:ss")
                .setPackagedLevel(0)
                .setStorage(null)
                .build();

        sLogger.i("INFO", "info");
        //sLogger.

        button = findViewById(R.id.button);
        editText = findViewById(R.id.editText);

        // set receiver listening
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // switch to another activity and display input text message
                Intent startIntent = new Intent(getApplicationContext(),  WelcomeScreenActivity.class);
                name = editText.getText().toString();
                startIntent.putExtra("package com.example.myapplication.INFORMATION", "Welcome to smart thermostat, " + name );
                startActivity(startIntent);
                finish();
                // display debug message on the console every time the button is clicked
                Log.d("User activity", "Button was clicked");

            }
        });
    }

    public Logger getLogger() {
        return sLogger;
    }
}
