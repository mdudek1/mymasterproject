package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WelcomeScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);
        // check if intent has extra information to be displayed and display it
        if (getIntent().hasExtra("package com.example.myapplication.INFORMATION")){
            TextView textView = (TextView) findViewById(R.id.textView);
            String text = getIntent().getExtras().getString("package com.example.myapplication.INFORMATION");
            textView.setText(text);
        }

        // configuration of a button returning to main activity
        Button backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(WelcomeScreenActivity.this, MainActivity.class);
                startActivity(backIntent);
            }
        });

        Button UsersListButton = findViewById(R.id.UsersListButton);
        UsersListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent UsersListIntent = new Intent(WelcomeScreenActivity.this,UsersListActivity.class);
                startActivity(UsersListIntent);

            }
        });
    }
}
