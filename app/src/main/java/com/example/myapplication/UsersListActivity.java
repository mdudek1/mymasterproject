package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.adapters.UsersAdapter;
import com.example.model.UserModel;
import com.example.user.User;

import java.util.ArrayList;
import java.util.List;

public class UsersListActivity extends Activity {

    Button btnAddUser;
    Button btnGetUserSList;
    List<User> users = new ArrayList<User>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_users_list);
            users = new HttpRequestUserList().execute().get();
            ListView  listViewUsers = (ListView) findViewById(R.id.listViewUser);
            UsersAdapter adapter = new UsersAdapter(users, getApplicationContext());
            listViewUsers.setAdapter(adapter);
            Button btnAddUser = findViewById(R.id.btnAddUser);
            btnAddUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(UsersListActivity.this, AddActivity.class );
                    startActivity(intent);
                }
            });

        }catch (Exception e){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(e.getMessage());
            builder.create().show();
        }
    }
    private static class HttpRequestUserList extends AsyncTask<Void,Void, List<User>>{

        @Override
        protected List<User> doInBackground(Void... params) {
            UserModel userModel = new UserModel();
            return userModel.read();
        }

        @Override
        protected void onPostExecute(List<User> users) {
            super.onPostExecute(users);
        }
    }

}
