package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.model.UserModel;
import com.example.user.User;

public class AddActivity extends Activity {

    private EditText editTextName;
    private EditText editTextSetHour;
    private EditText editTextSetTemperature;
    private Button buttonSave;
    private Button buttonCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_add);
            editTextName = findViewById(R.id.editTextName);
            editTextSetHour = findViewById(R.id.editTextSetHour);
            editTextSetTemperature = findViewById(R.id.editTextSetTemperature);
            buttonSave = findViewById(R.id.buttonSave);
            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        User user = new User();
                        user.setName(editTextName.getText().toString());
                        user.setSetHour(Double.parseDouble(editTextSetHour.getText().toString()));
                        user.setSetTemperature(Integer.parseInt(editTextSetTemperature.getText().toString()));
                        boolean result = new HttpRequestAdd().execute(user).get();
                        if(result) {
                            Intent intent = new Intent(AddActivity.this, UsersListActivity.class);
                            startActivity(intent);
                        }else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                            builder.setMessage("Add failed");
                            builder.create().show();
                        }

                    }catch (Exception e){
                        AlertDialog.Builder builder = new AlertDialog.Builder(AddActivity.this);
                        builder.setMessage(e.getMessage());
                        builder.create().show();
                    }
                }
            });
            buttonCancel = findViewById(R.id.buttonCancel);
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AddActivity.this, UsersListActivity.class);
                    startActivity(intent);
                }
            });
        }catch (Exception e){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(e.getMessage());
            builder.create().show();
        }
    }

    private static class HttpRequestAdd extends AsyncTask <User, Void,Boolean >{
        @Override
        protected Boolean doInBackground(User... users) {
            UserModel userModel = new UserModel();
            return userModel.create(users[0]);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }
}
