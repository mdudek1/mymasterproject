package com.example.config;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

public class ConfigReader{
    //declaration of class members
    private final Map<String, String> paramList;

    //constructor in which we open file


   /* public ConfigReader() {
        System.out.println("Konstruktor");
    }*/

    public ConfigReader(Map<String, String> paramList, Context context) {

        this.paramList = paramList;

        //AssetManager assetManager = context.getAssets();

        //opening file stream with try with resource
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open("Config.ini")))) {
            //reading line by line with splitting after first space
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                String[] s = line.split(" ", 2);
                if (s.length < 2 || s.length > 2)
                    continue;
                //displaying parameters and values and adding them to map containing pairs
                System.out.println("Parameter: " + s[0]);
                System.out.println("Value: " + s[1]);
                paramList.put(s[0], s[1]);
            }
            // displaying stack trace if file is not found
       // try {
        //InputStream in = assetManager.open("Config.ini", AssetManager.ACCESS_BUFFER);
        } catch (IOException e) {
            e.printStackTrace();
          //throw new RuntimeException(e);
        }
          //paramList.forEach((parameter, value) -> System.out.println("Name of the parameter: " + parameter + " Value: " + value));
    }

    public Map<String, String> getParamList() {
        return paramList;
    }

    @Override
    public String toString() {
        return "ConfigReader{" +
                "paramList=" + paramList +
                '}';
    }

    @NonNull
    public String getValue(String parameter) {
        return paramList.get(parameter);
    }

}
